function numbersToWords()
{   
    let one9Array = ["","one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"];
    let eleven20Array = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty"];
    let Tenths = ["","ten", "twenty", "thirty", "forty", "fifty", "sixy", "seventy", "eighty", "ninety"];

    let resultString = "";
    for(let i = 1; i <= 1000; i++)
    {
        if( i <= 10)
        {
            resultString += one9Array[i] + ", ";
        }
        else if(i >= 11 && i <= 20)
        {
            resultString += eleven20Array[i - 10] + ", ";
        }
        else
        {
            let currentI = String(i);  //turn 21 into "21" or 200 into "200" t
            if (i < 100)
            {
                let tenth = Number(currentI[0]);    //(2) from "21"  and turn into number again
                let ones = Number(currentI[1]);    //(1) from "21"  and turn into number again

                resultString += Tenths[tenth] + "-" + one9Array[ones] + ", ";
            }
            else if(i < 1000)   //Spell out:  one hundred four
            {
                let hundredths = Number(currentI[0]);   //1   from "102"
                let tenth = Number(currentI[1]);        // 0  from "102"
                let ones = Number(currentI[2]);         //  2 from "102"
                resultString += one9Array[hundredths] + "-hundred ";

                if(tenth == 1)
                {
                  resultString += eleven20Array[ones] + ", ";
                }
                else if (tenth > 1)
                {
                  resultString += Tenths[tenth] + "_" + one9Array[ones] + ", ";
                }    
            }
            else 
            {
              resultString += "one-thousand!";
            }
        }
    }
    document.write(resultString);   
}
numbersToWords();
